from flask import Flask, jsonify
from flask import request
import os
import tensorflow as tf
import PIL
import numpy as np
from tensorflow.keras.preprocessing import image
from datetime import datetime
from functions import SendNotif,loadPictures,loadTokens, reclamation,registerAgent,SaveReport,registerCitizen, loadLastPicture

app = Flask(__name__,static_folder='upload_folder',)
model = tf.keras.models.load_model('./model.h5')
categories = ["safe","fire"]
@app.route('/AI', methods=['POST'])
def api():
  print("hello")
  file = request.files['imageFile']
  filename = "test.jpg"
  timeNow=datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
  print("hellotimeNow")
  print(timeNow)
  file1=file
  file.save(os.path.join('upload_folder', "test.jpg"))
  file1.save(os.path.join('upload_folder', f"drone_{timeNow}.jpg"))
  print("timeNow")
  filenamefinal = "./upload_folder/drone_"+timeNow+".jpg"
  img = image.load_img("./upload_folder/test.jpg", target_size=(224, 224))
  print("img load")
  img_array = image.img_to_array(img)
  print("timeNow2")
  img_batch = np.expand_dims(img_array, axis=0)
  pred = model.predict(img_batch)
  print("timeNow3")
  max_index = np.argmax(pred)
  print(categories[max_index])
  print("timeNow4")
  if(max_index == 0):
    #nothing
    print("everything is ok")
    # Save Picture to Database
  else:
    #load from database tokens
    print("hi")
    registration_token = loadTokens()
    print(registration_token)
    SendNotif(title="Alert",message="Be Careful!!! there's a fire",registration_token=registration_token)
  SaveReport(max_index,f"drone_{timeNow}.jpg",timeNow)
  return jsonify({"response":"ok"})

@app.route('/registerToken', methods=['POST'])
def registerToken():
  content = request.get_json()
  token = content["token"]
  role=content["role"]
  print(token)
  print(role)
  if role == 0:
    print("citizen")
    # put token in citizen 
    registerCitizen(token)
  else:
    # put tiken in agent
    registerAgent(token)
  #save data in the database
  return jsonify({"response":"ok"})

@app.route('/picture', methods=['GET'])
def getLastPicture():
  #load last picture from database
  # loadLastPicture()
  print("hi")
  return {
    "picture":"test.jpg"
  }

@app.route('/pictures', methods=['GET'])
def getPictures():
  loadPictures()
  #load list of pictures from database
  print("hi")

@app.route('/reclamation', methods=['POST'])
def Reclamation():
  #send reclamation to database
  file = request.files['imageFile']
  filename = f"imgPhone_{datetime.now()}.jpg"
  file.save(os.path.join('upload_folder', filename))
  reclamation(filename, 0, 0, 0)

  img = image.load_img(os.path.join('upload_folder', filename), target_size=(224, 224))
  img_array = image.img_to_array(img)
  print("timeNow2")
  img_batch = np.expand_dims(img_array, axis=0)
  pred = model.predict(img_batch)
  print("timeNow3")
  max_index = np.argmax(pred)
  print(categories[max_index])
  print("timeNow4")
  if(max_index == 0):
    #nothing
    print("everything is ok")
    # Save Picture to Database
  else:
    #load from database tokens
    print("hi")
    registration_token = loadTokens()
    print(registration_token)
    SendNotif(title="Alert",message="a Citizen reclame there's a fire",registration_token=registration_token)

  return jsonify({"response":"ok"})


if __name__ == "_main_":
    app.run(host='0.0.0.0', port=5000)