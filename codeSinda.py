import requests

URL = "http://192.168.243.107:5000/AI"
TEST_IMAGE_FILE_PATH = "./img/fire.jpg"
# TEST_IMAGE_FILE_PATH = r"luffy.jpg"
image_file = open(TEST_IMAGE_FILE_PATH, "rb")
values = {"file": (TEST_IMAGE_FILE_PATH, image_file, 'image/*')}
print("test")
# get predicted word from server
response = requests.post(URL, files=values)

data = response.json()
print(f"The predicted response is {data['response']}")